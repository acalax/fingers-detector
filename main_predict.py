import detector
import properties
import predictor

predictor = predictor.Predictor()
predictor.set_top_bottom_model('model_top.json', 'model_top.h5')
predictor.set_left_right_model('model_left.json', 'model_left.h5')

detector = detector.Detector(1024)
detector.evaluate_images(predictor)
