from imutils.video import VideoStream
from imutils.video import FPS
import imutils
import time
import cv2
from matplotlib import pyplot as plt
import properties
import numpy as np
from random import randint
import os
import utils
import predictor


class Detector:

    def __init__(self, window_size):
        self.window_size = window_size
        self.is_recording = False
        # create dir structure if not existing
        utils.create_directories()
        # delete previously captured files
        utils.delete_sample_files()

    def _create_images_border(self, orig, border):
        if border == 'top':
            top_border = orig[0:properties.capture_border_size,
                              0:orig.shape[1]]
            return top_border
        elif border == 'bottom':
            bottom_border = orig[orig.shape[0] -
                                 properties.capture_border_size: orig.shape[0], 0:orig.shape[1]]
            return bottom_border
        elif border == 'left':
            left_border = orig[0:orig.shape[0],
                               0:properties.capture_border_size]
            return left_border
        elif border == 'right':
            right_border = orig[0:orig.shape[0], orig.shape[1] -
                                properties.capture_border_size:orig.shape[1]]
            return right_border

    def _create_top_bottom_border(self, orig):
        border_image = np.append(self._create_images_border(orig, 'top'),
                                 self._create_images_border(orig, 'bottom'), axis=0)

        border_image = cv2.resize(
            border_image, properties.capture_top_bottom_border_dim, interpolation=cv2.INTER_AREA)
        return border_image

    def _create_left_right_border(self, orig):
        border_image = np.append(self._create_images_border(orig, 'left'),
                                 self._create_images_border(orig, 'right'), axis=0)

        border_image = cv2.resize(
            border_image, properties.capture_left_right_border_dim, interpolation=cv2.INTER_AREA)
        return border_image

    def _save_frame(self, frame, count, save_path_top_bottom, save_path_left_right):
        # capture rectangle dimensions
        CAPT_WIDTH = properties.capture_rect_width
        CAPT_HEIGHT = properties.capture_rect_heigth

        if count % properties.capture_frame_interval == 0:

            # capture area
            frame = frame[properties.capture_rect_pos_y + 20:properties.capture_rect_pos_y + CAPT_HEIGHT - 25,
                          properties.capture_rect_pos_x + 10:properties.capture_rect_pos_x + CAPT_WIDTH - 25]

            skip_for_blur, blur_value = self._is_blurry(frame)
            if skip_for_blur:
                print(
                    'Blurry image detected - skipping it : score {}'.format(blur_value))
            else:
                processed_top_bottom_frame = self._create_top_bottom_border(
                    frame)
                processed_left_right_frame = self._create_left_right_border(
                    frame)

                rnd_number = randint(0, 1000)
                # image name random to avoid overwritings
                cv2.imwrite(save_path_top_bottom + '/frame{}_{}.jpg'.format(count,
                                                                            rnd_number), processed_top_bottom_frame)
                cv2.imwrite(save_path_left_right + '/frame{}_{}.jpg'.format(count,
                                                                            rnd_number), processed_left_right_frame)

                cv2.imshow("Processed top-bottom", processed_top_bottom_frame)
                cv2.imshow("Processed left-right", processed_left_right_frame)

    def _is_blurry(self, image):
        # compute the Laplacian of the image and then return the focus
            # measure, which is simply the variance of the Laplacian
        var_laplacian = cv2.Laplacian(image, cv2.CV_64F).var()
        return var_laplacian <= properties.capture_blurry_threshold, var_laplacian

    def _show_prediction_outcomes(self, prediction_top_bottom, processed_top_bottom_frame,
                                  prediction_left_right, processed_left_right_frame):
        if prediction_top_bottom == 1:
            cv2.rectangle(processed_top_bottom_frame,
                          (10, 10), (30, 30), (0, 255, 0), 2)
        else:
            cv2.rectangle(processed_top_bottom_frame,
                          (10, 10), (30, 30), (0, 0, 255), 2)

        if prediction_left_right == 1:
            cv2.rectangle(processed_left_right_frame,
                          (10, 10), (30, 30), (0, 255, 0), 2)
        else:
            cv2.rectangle(processed_left_right_frame,
                          (10, 10), (30, 30), (0, 0, 255), 2)

    def _create_images_border(self, orig, border):
        if border == 'top':
            top_border = orig[0:properties.capture_border_size,
                              0:orig.shape[1]]
            return top_border
        elif border == 'bottom':
            bottom_border = orig[orig.shape[0] -
                                 properties.capture_border_size: orig.shape[0], 0:orig.shape[1]]
            return bottom_border
        elif border == 'left':
            left_border = orig[0:orig.shape[0],
                               0:properties.capture_border_size]
            return left_border
        elif border == 'right':
            right_border = orig[0:orig.shape[0], orig.shape[1] -
                                properties.capture_border_size:orig.shape[1]]
            return right_border

    def _create_top_bottom_border(self, orig):
        border_image = np.append(self._create_images_border(orig, 'top'),
                                 self._create_images_border(orig, 'bottom'), axis=0)
        border_image = cv2.resize(
            border_image, (415, 60), interpolation=cv2.INTER_AREA)
        return border_image

    def _create_left_right_border(self, orig):
        border_image = np.append(self._create_images_border(orig, 'left'),
                                 self._create_images_border(orig, 'right'), axis=0)
        border_image = cv2.resize(
            border_image, (30, 510), interpolation=cv2.INTER_AREA)
        return border_image

    def _process_and_reshape(self, frame):
        processed_top_bottom_frame = self._create_top_bottom_border(frame)
        processed_left_right_frame = self._create_left_right_border(frame)

        if len(processed_top_bottom_frame.shape) == 3:
            reshaped_processed_top_bottom_frame = processed_top_bottom_frame.reshape(1, processed_top_bottom_frame.shape[0] *
                                                                                     processed_top_bottom_frame.shape[1] * processed_top_bottom_frame.shape[2])

            reshaped_processed_left_right_frame = processed_left_right_frame.reshape(1, processed_left_right_frame.shape[0] *
                                                                                     processed_left_right_frame.shape[1] * processed_left_right_frame.shape[2])
        else:
            reshaped_processed_top_bottom_frame = processed_top_bottom_frame.reshape(1, processed_top_bottom_frame.shape[0] *
                                                                                     processed_top_bottom_frame.shape[1])

            reshaped_processed_left_right_frame = processed_left_right_frame.reshape(1, processed_left_right_frame.shape[0] *
                                                                                     processed_left_right_frame.shape[1])

        return reshaped_processed_top_bottom_frame, reshaped_processed_left_right_frame,\
            processed_top_bottom_frame, processed_left_right_frame

    def _show_prediction_outcomes(self, prediction_top_bottom, processed_top_bottom_frame, prediction_left_right,
                                  processed_left_right_frame):
        if prediction_top_bottom == 1:
            cv2.rectangle(processed_top_bottom_frame,
                          (10, 10), (30, 30), (0, 255, 0), 2)
        else:
            cv2.rectangle(processed_top_bottom_frame,
                          (10, 10), (30, 30), (0, 0, 255), 2)

        if prediction_left_right == 1:
            cv2.rectangle(processed_left_right_frame,
                          (10, 10), (30, 30), (0, 255, 0), 2)
        else:
            cv2.rectangle(processed_left_right_frame,
                          (10, 10), (30, 30), (0, 0, 255), 2)

    def _evaluate_frame(self, frame, count, predictor):
        CAPT_WIDTH = properties.capture_rect_width
        CAPT_HEIGHT = properties.capture_rect_heigth

        prediction_top_bottom = 0
        prediction_left_right = 0

        if count % properties.capture_frame_interval == 0:

            if properties.capture_crop == 1:
                frame = frame[properties.capture_rect_pos_y + 20:properties.capture_rect_pos_y + CAPT_HEIGHT - 25,
                              properties.capture_rect_pos_x + 10:properties.capture_rect_pos_x + CAPT_WIDTH - 25]

            skip_for_blur, blur_value = self._is_blurry(frame)
            if skip_for_blur:
                print(
                    'Blurry image detected - skipping it : score {}'.format(blur_value))
            else:
                reshaped_processed_top_bottom_frame, reshaped_processed_left_right_frame,\
                    processed_top_bottom_frame, processed_left_right_frame = self._process_and_reshape(
                        frame)

                prediction_top_bottom, prediction_left_right = predictor.predict(reshaped_processed_top_bottom_frame,
                                                                                 reshaped_processed_left_right_frame)

                rnd_number = randint(0, 1000)
                cv2.imwrite(properties.data_path + 'test_top_bottom/frame{}_{}_{}.jpg'.format(count, rnd_number, prediction_top_bottom),
                            processed_top_bottom_frame)
                cv2.imwrite(properties.data_path + 'test_left_right/frame{}_{}_{}.jpg'.format(count, rnd_number, prediction_left_right),
                            processed_left_right_frame)

                self._show_prediction_outcomes(
                    prediction_top_bottom, processed_top_bottom_frame, prediction_left_right, processed_left_right_frame)

                # show frames
                cv2.imshow('Frame - top bottom', processed_top_bottom_frame)
                cv2.imshow('Frame - left right', processed_left_right_frame)

        return prediction_top_bottom + prediction_left_right

    # allows to capture images for models training
    def capture_frames(self, capture_rect_width, capture_rect_heigth):
        vs = VideoStream(src=0).start()
        time.sleep(1.0)

        count = 0

        while True:
            frame = vs.read()

            if frame is None:
                break

            frame = imutils.resize(frame, width=self.window_size)

            key = cv2.waitKey(1) & 0xFF
            if key == ord('c'):
                self.is_recording = not self.is_recording
            elif key == ord("q"):
                break

            if self.is_recording:
                self._save_frame(frame, count, properties.capture_top_bottom_sample_path,
                                 properties.capture_left_right_sample_path)
                cv2.putText(frame, 'Recording samples', (30, 30),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 0, 0), 2)
                count += 1

            # draw rect
            cv2.rectangle(frame, properties.capture_rect_dim, (20 +
                                                               capture_rect_width, 20+capture_rect_heigth), (255, 0, 0), 2)
            cv2.imshow("Live", frame)

        vs.stop()
        cv2.destroyAllWindows()

    # allows to evaluate images using a classifier
    def evaluate_images(self, predictor):
        CAPT_WIDTH = properties.capture_rect_width
        CAPT_HEIGHT = properties.capture_rect_heigth

        is_evaluating = False

        vs = VideoStream(src=0).start()
        time.sleep(1.0)

        count = 0

        found_sums = []
        found_sum = 0

        while True:
            frame = vs.read()

            if frame is None:
                break

            key = cv2.waitKey(1) & 0xFF
            if key == ord('e'):
                is_evaluating = not is_evaluating
            elif key == ord("q"):
                break

            frame = imutils.resize(frame, width=self.window_size)
            
            if is_evaluating:
                cv2.putText(frame, 'Evaluating samples', (30, 30),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 0, 0), 2)
                found_sum = self._evaluate_frame(
                    frame, count, predictor)
                if found_sum == 2:
                    found_sums.append(found_sum)
                    # save candidate image
                    cv2.imwrite(properties.data_path +
                                'output/{}.jpg'.format(str(count)), frame)

                if len(found_sums) == properties.capture_ok_frames:
                    candidate_image = cv2.imread(
                        properties.data_path + 'output/{}.jpg'.format(str(count)))
                    cv2.imshow("Best candidate", candidate_image)
                    is_evaluating = False
                    found_sums = []
                    found_sum = 0

                count += 1

            # draw react
            cv2.rectangle(frame, (50, 50), (20+CAPT_WIDTH,
                                            20+CAPT_HEIGHT), (255, 0, 0), 2)

            cv2.imshow("Live evaluation", frame)

        vs.stop()
        cv2.destroyAllWindows()


# utils.delete_sample_files()
